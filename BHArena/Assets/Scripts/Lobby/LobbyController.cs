using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LobbyController : MonoBehaviourPun
{
    [SerializeField] private VerticalLayoutGroup listOfPlayers;
    [SerializeField] private PhotonView PhotonView;
    
    // Start is called before the first frame update
    void Start()
    {    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        listOfPlayers.GetComponent<ListOfPlayers>().RefreshList();
    }


}
