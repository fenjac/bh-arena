using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;

public class ListOfPlayers : MonoBehaviourPun
{
    [SerializeField] VerticalLayoutGroup listOfPlayers;
    [SerializeField] GameObject myPrefab;

    private GameObject newPrefab;
    private TextMeshProUGUI tmp;
    private Dictionary<string, TextMeshProUGUI> tmps = new Dictionary<string, TextMeshProUGUI>();
    private Dictionary<string, Player> addedPlayers = new Dictionary<string, Player>();

    public void RefreshList()
    {
        var list =  listOfPlayers.GetComponents<TextMeshProUGUI>();

        foreach (Player player in PhotonNetwork.PlayerList)
        {
            
            if (!addedPlayers.ContainsKey(player.NickName))
            {
                addedPlayers.Add(player.NickName, player);
                CreateItemForPlayer(player);
            }
        }
    }

    public void OnPhotonPlayerDisconnected(Player player)
    {
        Debug.Log("player: " + player.NickName + " disconnected");
        addedPlayers.Remove(player.NickName);
        TextMeshProUGUI tmp = tmps[player.NickName];
        Component.Destroy(tmp);
        tmps.Remove(player.NickName);
        var t = listOfPlayers.GetComponent<RectTransform>();
        LayoutRebuilder.ForceRebuildLayoutImmediate(listOfPlayers.GetComponent<RectTransform>());

    }

    private void CreateItemForPlayer(Player player)
    {
        Debug.Log("Create item for player: " + player.NickName);
        newPrefab = Instantiate(myPrefab, new Vector3(0, 0, 0), Quaternion.identity);        
        tmp = newPrefab.GetComponent<TextMeshProUGUI>();
        tmp.text = player.NickName;
        tmp.transform.parent = listOfPlayers.transform;
        tmps.Add(player.NickName, tmp);
    }
}
